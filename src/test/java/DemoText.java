import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class DemoText {
    public static void main(String[] args) {
        System.out.println(getUserName("http://127.0.0.1:8080/myBatisDemo/getHead"));
    }
    public static String getUserName(String getUrl) {
        URL url = null;
        try {
            url = new URL(getUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setReadTimeout(3000);
            con.setReadTimeout(3000);
            con.connect();
            InputStream is = con.getInputStream();
            String username = con.getHeaderField("jiwang") == null ? "" : con.getHeaderField("jiwang");
            System.out.println(username);
            // LOGGER.debug("http请求获取username==> " + username);
            for (Map.Entry<String, List<String>> stringListEntry : con.getHeaderFields().entrySet()) {
                System.out.println("所有头信息" +stringListEntry.getKey()+"========"+stringListEntry.getValue());
              //  LOGGER.debug("所有头信息" +stringListEntry.getKey()+"========"+stringListEntry.getValue());
            }

            if (!username.isEmpty()) {
                con.disconnect();
                return username;
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\r\n");
            }
            if (null != br) {
                br.close();
            }
            if (null != is) {
                is.close();
            }
            //关闭连接
            con.disconnect();
            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
