package com.testdemo.common;

import javax.sql.DataSource;

public interface PoolingDataSourcePrivider {
	public static int MAX_IDLE = 7;
	public static int MAX_CONNECTION = 384;
	public static int START_CONNECTION = 2;
	public DataSource CreatePoolingDataSource(final String DriverClass, final String url, final int minIdle ,final int maxIdle); 
}
