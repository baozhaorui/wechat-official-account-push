package com.testdemo.common;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class DirectDataSource implements DataSource {
	private final String connUrl ;
	public DirectDataSource(final String connstr) {
		this.connUrl = connstr;
	}
	
	PrintWriter _logger = null;
	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return _logger;
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {
		_logger = out;
	}

	int _timeout = 0;
	@Override
	public void setLoginTimeout(int seconds) throws SQLException {
		_timeout = seconds;
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return _timeout;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return null;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		T localObject;
	    try {
	      localObject = iface.cast(this);
	    }
	    catch (ClassCastException localClassCastException)
	    {
	      throw new SQLException(localClassCastException.getMessage(), localClassCastException);
	    }
	    return localObject;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return iface.isInstance(this);
	}

	@Override
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(connUrl);
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {
		return DriverManager.getConnection(connUrl, username, password);
	}

}
