package com.testdemo.common;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;


public final class JdbcPool{
	private static PoolingDataSourcePrivider privider; 
	public static void setTestPrivider() {
		privider = new DirectDataSoucePrivider();
		System.out.println("Privider change to =>" + privider.getClass().getName());
	}
	
	static {
		while(true) {
			final ClassLoader loader = JdbcPool.class.getClassLoader();
			Class<?> driverClass = null;
			try {
				driverClass = loader.loadClass("org.apache.tomcat.jdbc.pool.DataSource");
			} catch (Throwable e) {
				e.printStackTrace();
			}
			if(null != driverClass) {
				privider = new TomcatJdbcPoolDataSoucePrivider();
				break;
			}
			
			privider = new DirectDataSoucePrivider();
			break;
		}
		System.out.println("Loader Jdbc Pool Privider=>" + privider.getClass().getName());
	}
	private static final int def_minIdle = 0;
	//private static final int def_minIdle = 64;
	private static final int def_maxIdle = 7;
	
	
	private static final String jdbc_sqlserver_ds = "com.microsoft.sqlserver.jdbc.SQLServerXADataSource";
	private static final String jdbc_postgresql_ds = "org.postgresql.Driver";
	private static final String jdbc_kingbasesql_ds = "com.kingbase8.Driver";

	public static ConcurrentHashMap<String, DataSource> namemap = new ConcurrentHashMap<String, DataSource>();
	public static ConcurrentHashMap<String, String> dbnamemap = new ConcurrentHashMap<String, String>();
	
	public static Connection getConnection(String name) throws SQLException{
		if(null == name || name.isEmpty()) return null;
		DataSource ds = namemap.get(name);
		if(null == ds) return null;
		return ds.getConnection();
	}
	
	public static Connection tryGetConnection(String connstr)  throws SQLException {
		Connection conn = getConnection(connstr);
		if(null != conn ) return conn;

		if(connstr.startsWith("dbo-")) {
			throw new RuntimeException("connstr is not config:" + connstr);
		} else if(connstr.startsWith("jdbc:")) {
			return cacheAndgetUrlConnection(connstr);
		}
		throw new RuntimeException("connstr Type is Unknown:" + connstr);
	}
	
	public static Connection cacheAndgetUrlConnection(final String url)  throws SQLException {
		String driverclass;
		if (url.startsWith("jdbc:sqlserver:") ) {
			driverclass= jdbc_sqlserver_ds;
		} else if (url.startsWith("jdbc:postgresql") ) {
			driverclass= jdbc_postgresql_ds;
		} else if (url.startsWith("jdbc:kingbase8")){
			driverclass= jdbc_kingbasesql_ds;
		}else {
			final Driver  dr = DriverManager.getDriver(url);
			if ( null == dr ) return null;
			driverclass = dr.getClass().getName();
		}
		DataSource ds = addDataSource(url, driverclass, url, def_minIdle, def_maxIdle );
		return ds.getConnection();
	}
	
	public static DataSource addDataSource(final String name, final String url) throws SQLException {
		return addDataSource(name, url, def_minIdle, def_maxIdle );
	}
	
	public static DataSource addDataSource(final String name, final String url, final int minIdle ,final int maxIdle) throws SQLException {
		String driverclass;
		if (url.startsWith("jdbc:sqlserver:") ) {
			driverclass= jdbc_sqlserver_ds;
		} else {
			final Driver  dr = DriverManager.getDriver(url);
			if ( null == dr ) return null;
			driverclass = dr.getClass().getName();
		}
		return addDataSource(name, driverclass, url, minIdle, maxIdle );
	}
	
	public static DataSource addDataSource(final String name, final String DriverClass, final String url, final int minIdle ,final int maxIdle) {
		final DataSource pds = privider.CreatePoolingDataSource(DriverClass, url, minIdle, maxIdle);
		namemap.put(name, pds);
		return pds;
	}
	
	public static void init(Map<String, String> config) {
		System.out.println(JdbcPool.class.getName() + "::init()");
		for(Entry<String, String> ent  : config.entrySet()) {
			String key = (String) ent.getKey();
			String[] keys = key.split(":");
			if(! key.startsWith("dbo-") || keys.length < 2) continue;
			System.out.println(key + " is Found");
			String url = ent.getValue();
			String name = keys[0];
			
			int minIdle = def_minIdle;
			int maxIdle = def_maxIdle; 
			
			if( keys.length > 2) minIdle = Integer.parseInt(keys[2]);
			if( keys.length > 3) maxIdle = Integer.parseInt(keys[3]);
			//if( keys.length > 4) exAct =  Integer.parseInt(keys[4]);
			
			String DriverClass = keys[1];
 
			System.out.println(name  + " \t minIdle=" + minIdle + "\t maxIdle=" + maxIdle);
 
			JdbcPool.addDataSource(name, DriverClass, url, minIdle, maxIdle);
		}
		System.out.println(JdbcPool.class.getName() + "::init()END");
	}
	
	public static void destroy(){
		namemap.clear();
		System.out.println(JdbcPool.class.getName() + "::destroy()");
	}

	public static void remove(String key){
		namemap.remove(key);
	}
}
/*
<init-param>
<param-name>dbo-bbs:com.microsoft.sqlserver.jdbc.SQLServerXADataSource</param-name>
<param-value>jdbc:sqlserver://host:port;DATABASE=dbname;user=%usr%;password=%pwd%;encrypt=false</param-value>
</init-param>
 */










