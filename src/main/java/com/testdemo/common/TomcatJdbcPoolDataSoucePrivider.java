package com.testdemo.common;

import org.apache.tomcat.jdbc.pool.PoolProperties;

import javax.sql.DataSource;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TomcatJdbcPoolDataSoucePrivider implements PoolingDataSourcePrivider {
	@Override
	public DataSource CreatePoolingDataSource(String DriverClass, String url, int minIdle, int maxIdle) {
		if(null == url || url.isEmpty()) {
			throw new RuntimeException("Database Url is Empty");
		}
		
		PoolProperties p = new PoolProperties();
		
		Class<?> dricls = null;
		if(null != DriverClass && !DriverClass.isEmpty()) {
			try {
				dricls = Class.forName(DriverClass);
			} catch (ClassNotFoundException e) {}
		}
		
		if(null == dricls) {
			try {
				Driver dri = DriverManager.getDriver(url);
				DriverClass = dri.getClass().getName();
				p.setUrl(url);
		        p.setDriverClassName(DriverClass);
			} catch (SQLException e) {
				throw new RuntimeException("Driver is Unknown for:" + url, e);
			}
		} else {
			if(DataSource.class.isAssignableFrom(dricls)){
				try {
					DataSource ds = (DataSource) (dricls.newInstance());
					dricls.getMethod("setURL", String.class).invoke(ds, url);
					p.setDataSource(ds);
				} catch (final Exception e) {
					throw new RuntimeException("Driver load failed:" + DriverClass, e);
				}
			} else if (Driver.class.isAssignableFrom(dricls)) {
				p.setUrl(url);
		        p.setDriverClassName(DriverClass);
			} else {
				throw new RuntimeException("Unknown Driver Type:" + DriverClass);
			}
		}
		
        //p.setUsername("root");
        //p.setPassword("password");
		p.setDefaultAutoCommit(true);
		//p.setDefaultTransactionIsolation(defaultTransactionIsolation);
        p.setJmxEnabled(true);
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxIdle(maxIdle);
        p.setMinIdle(minIdle);
        p.setMaxActive(MAX_CONNECTION);
        p.setInitialSize(START_CONNECTION);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(600);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setLogAbandoned(false);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
          "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
          "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
        org.apache.tomcat.jdbc.pool.DataSource datasource = new org.apache.tomcat.jdbc.pool.DataSource();
        datasource.setPoolProperties(p);
		return datasource;
	}
	
}
