package com.testdemo.common;

import javax.sql.DataSource;

public class DirectDataSoucePrivider implements PoolingDataSourcePrivider {
	@Override
	public DataSource CreatePoolingDataSource(String DriverClass, String url, int minIdle, int maxIdle) {
		return new DirectDataSource(url);
	}
}
