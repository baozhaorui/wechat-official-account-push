package com.testdemo.service.impl;

import com.testdemo.mapper.MybatisDemoMapper;
import com.testdemo.pojo.User;
import com.testdemo.service.MyBatisDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MybatisDemoServiceImpl implements MyBatisDemoService {
    @Autowired
    private MybatisDemoMapper mybatisDemoMapper;

    @Override
    public void addUser(List<User> users) {
        for (User user : users) {
            mybatisDemoMapper.insert(user);
        }
    }

}
