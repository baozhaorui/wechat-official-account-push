package com.testdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.testdemo.pojo.User;
import org.springframework.stereotype.Repository;

@Repository
public interface MybatisDemoMapper extends BaseMapper<User> {
}
