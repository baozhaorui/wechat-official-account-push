package com.testdemo.controller;

import com.testdemo.pojo.User;
import com.testdemo.service.MyBatisDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@RestController
@RequestMapping("/myBatisDemo")
public class MybatisDemoController {
    @Autowired
    private MyBatisDemoService myBatisDemoService;

    @RequestMapping("/addUser")
    public HttpHeaders addUser(@RequestBody List<User> users) {
        myBatisDemoService.addUser(users);
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("jiwang", "123456");
       return httpHeaders;
    }

    @RequestMapping("/getHead")
    public HttpHeaders addUser() {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("jiwang", "123456");
        return httpHeaders;
    }
    //获取用户名
    public String getUserName(String getUrl) {
        URL url = null;
        try {
            url = new URL(getUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setReadTimeout(3000);
            con.setReadTimeout(3000);
            con.connect();
            InputStream is = con.getInputStream();
            String username = con.getHeaderField("jiwang") == null ? "" : con.getHeaderField("username");
            System.out.println(username);
           // LOGGER.debug("http请求获取username==> " + username);
            /*for (Map.Entry<String, List<String>> stringListEntry : con.getHeaderFields().entrySet()) {
              //  LOGGER.debug("所有头信息" +stringListEntry.getKey()+"========"+stringListEntry.getValue());
            }*/

            if (!username.isEmpty()) {
                con.disconnect();
                return username;
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\r\n");
            }
            if (null != br) {
                br.close();
            }
            if (null != is) {
                is.close();
            }
            //关闭连接
            con.disconnect();
            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}
