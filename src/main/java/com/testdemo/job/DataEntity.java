package com.testdemo.job;

import lombok.Data;

@Data
public class DataEntity {//内容
    private String value;
    //字体颜色
    private String color;

    public DataEntity(String value, String color) {
        this.value = value;
        this.color = color;
    }
}
