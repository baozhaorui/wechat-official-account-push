package com.testdemo.job;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class WechatMsg {

    @Value("${address}")
    private String address;

    @Value("${toUser}")
    private String toUser;

    public static void main(String[] args) {
        WechatMsg wm = new WechatMsg();
        wm.SendWeChatMsg(wm.getToken());

    }
    /**
     * 获取token
     *
     * @return token
     */
    public String getToken() {
// 授予形式
        String grant_type = "client_credential";
//应用ID
        String appid = "wx81db82b11841963e";
//密钥
        String secret = "86b4b8e984b717b14be974f0aa5be30f";
// 接口地址拼接参数
        String getTokenApi = "https://api.weixin.qq.com/cgi-bin/token?grant_type=" + grant_type + "&appid=" + appid
                + "&secret=" + secret;
        String tokenJsonStr = doGetPost(getTokenApi, "GET", null);
        JSONObject tokenJson = JSONObject.parseObject(tokenJsonStr);
        String token = tokenJson.get("access_token").toString();
        System.out.println("获取到的TOKEN : " + token);
        return token;
    }
    /***
     * 发送消息
     *
     * @param token
     */
    public void SendWeChatMsg(String token) {
// 接口地址
        String sendMsgApi = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+token;
//openId
        String params= null;
        String toUser= null;
        try {
             toUser = YmlUtils.getValue("toUser");
            params = "id="+ YmlUtils.getValue("address");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//消息模板ID
        String template_id = "dW-qWjLpCtp0PT60uyV_dWWDXk3OzZ42K_D8Ju-HcGc";
//整体参数map
        Map<String, Object> paramMap = new HashMap<String, Object>();
//消息主题显示相关map
        Map<String, Object> dataMap = new HashMap<String, Object>();
//根据自己的模板定义内容和颜色
        String url="https://api.help.bj.cn/apis/weather2d/";
        String s = HttpRequestUtil.sendGet(url,params);
        JSONObject jsonObject1 = JSONObject.parseObject(s);
        String result = jsonObject1.getString("temp");
//        System.out.println(result);

        String url2= " http://open.iciba.com/dsapi/";
        String s1 = HttpRequestUtil.sendGet(url2, "");
        JSONObject jsonObject2 = JSONObject.parseObject(s1);
//        System.out.println(jsonObject2.getString("note"));
//        System.out.println(jsonObject2.getString("content"));

        dataMap.put("address",new DataEntity(jsonObject1.getString("city"),"#f1ccb8"));
        dataMap.put("weather",new DataEntity(jsonObject1.getString("temp"),"#f2debd"));
        dataMap.put("sentence",new DataEntity(jsonObject2.getString("note") ,"#b8d38f"));
        dataMap.put("English_sentence",new DataEntity(jsonObject2.getString("content"),"#ff9b6a"));
        paramMap.put("touser", toUser);
        paramMap.put("template_id", template_id);
        paramMap.put("data", dataMap);
        paramMap.put("url","tourl");
//需要实现跳转网页的，可以添加下面一行代码实现跳转
// paramMap.put("url","http://xxxxxx.html");
        System.out.println(doGetPost(sendMsgApi,"POST",paramMap));
    }
    /**
     * 调用接口 post
     * @param apiPath
     */
    public String doGetPost(String apiPath,String type,Map<String,Object> paramMap){
        OutputStreamWriter out = null;
        InputStream is = null;
        String result = null;
        try{
            URL url = new URL(apiPath);// 创建连接
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestMethod(type) ; // 设置请求方式
            connection.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式
            connection.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式
            connection.connect();
            if(type.equals("POST")){
                out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8"); // utf-8编码
                out.append(JSON.toJSONString(paramMap));
                out.flush();
                out.close();
            }
// 读取响应
            is = connection.getInputStream();
            int length = (int) connection.getContentLength();// 获取长度
            if (length != -1) {
                byte[] data = new byte[length];
                byte[] temp = new byte[512];
                int readLen = 0;
                int destPos = 0;
                while ((readLen = is.read(temp)) > 0) {
                    System.arraycopy(temp, 0, data, destPos, readLen);
                    destPos += readLen;
                }
                result = new String(data, "UTF-8"); // utf-8编码
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
